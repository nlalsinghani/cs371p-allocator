// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <vector>
#include <string>
#include <sstream>

#include "Allocator.hpp"
using namespace std;

// ----
// main
// ----

//creates vector of what to allocate
vector<int> createInput(istream & i) {
    string str;
    vector<int> numAllocate;
    while(getline(i,str) && !str.empty()) {
        int bytes = stoi(str);
        numAllocate.push_back(bytes);
    }
    return numAllocate;
}

//returns pointer to correct sentinel
int* deallocateHelper(char* memory, int usedBlocks) {
    int numBlocks = 0;
    int i = 0;
    while(i < 1000) {
        int sentinel = *(int*)(memory);
        if(sentinel < 0) {
            ++numBlocks;
            sentinel = sentinel * -1;
        }
        if(numBlocks == usedBlocks) {
            return (int*)(memory);
        }
        memory = memory + (8 + sentinel);
        i = i + (8 + sentinel);
    }
    return nullptr;
}

//used to output
void print(char* memory) {
    int i = 0;
    vector<int> b;
    while(i < 1000) {
        int sentinel = *(int*)(memory);
        b.push_back(sentinel);
        if(sentinel < 0)
            sentinel = sentinel * -1;
        memory = memory + (8 + sentinel);
        i = i + (8 + sentinel);
    }
    for(int i = 0; i < (int)b.size() - 1; i++) {
        cout<< b[i] << " ";
    }
    cout << b[b.size() - 1] << endl;
}

int main () {
    string str;
    getline(cin, str);
    int numTests = stoi(str);
    assert(numTests > 0 && numTests < 100);
    getline(cin,str);
    for (int i = 0; i < numTests; ++i) {
        vector<int> input = createInput(cin);
        my_allocator<double, 1000> allocator;
        for (int j = 0; j < (int)input.size(); ++j) {
            int x = input[j];
            if(x > 0 )
                allocator.allocate(x);
            else {
                int* b = deallocateHelper(allocator.getMemory(), -1 * x);
                if(b != nullptr) {
                    allocator.deallocate((double*)(b + 1), (*b) / -8);
                }
            }
        }
        print(allocator.getMemory());
    }
    return 0;
}

