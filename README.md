# CS371p: Object-Oriented Programming Allocator Repo

* Name: Nirav Lalsinghani

* EID: NVL225

* GitLab ID: @nlalsinghani

* HackerRank ID: nirav_lalsingha1

* Git SHA: 3df67359

* GitLab Pipelines: https://gitlab.com/nlalsinghani/cs371p-allocator/-/pipelines

* Estimated completion time: 10 hours

* Actual completion time: 14 hours

* Comments: extension really helped
