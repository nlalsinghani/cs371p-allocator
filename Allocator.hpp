// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

using namespace std;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;}                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}

    public:
        // --------
        // typedefs
        // --------

        using      value_type = T;

        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;

    public:
        // ---------------
        // iterator
        // over the blocks
        // ---------------

        class iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const iterator& lhs, const iterator& rhs) {
                return lhs._p == rhs._p;
            }

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs == rhs);}

            private:
                // ----
                // data
                // ----

                int* _p;

            public:
                // -----------
                // constructor
                // -----------

                iterator (int* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                int& operator * () const {
                    return *_p;}

                // -----------
                // operator ++
                // -----------

                iterator& operator ++ () {
                    _p++;
                    return *this;}

                // -----------
                // operator ++
                // -----------

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                iterator& operator -- () {
                    _p--;
                    return *this;}

                // -----------
                // operator --
                // -----------

                iterator operator -- (int) {
                    iterator x = *this;
                    --*this;
                    return x;}};

        // ---------------
        // const_iterator
        // over the blocks
        // ---------------

        class const_iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
                return lhs._p == rhs._p;
            }                                                       

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
                return !(lhs == rhs);}

            private:
                // ----
                // data
                // ----

                const int* _p;

            public:
                // -----------
                // constructor
                // -----------

                const_iterator (const int* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                const int& operator * () const {
                    return *_p;
                }

                // -----------
                // operator ++
                // -----------

                const_iterator& operator ++ () {
                    _p++;
                    return *this;}

                // -----------
                // operator ++
                // -----------

                const_iterator operator ++ (int) {
                    const_iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                const_iterator& operator -- () {
                    _p--;
                    return *this;}

                // -----------
                // operator --
                // -----------

                const_iterator operator -- (int) {
                    const_iterator x = *this;
                    --*this;
                    return x;}};

    private:
        // ----
        // data
        // ----

        char a[N];

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * checks to see if the memory blocks are valid. Used in allocation, deallocation
         * and destroy. Must be valid in order to proceed.
         */
        bool valid () const {
            int lhs = (*this)[0];
            std::size_t i = 0;
            while(i < N){
                if(lhs > 0 && i > 0 && (*this)[i - 4] > 0){
                    return false;
                }
                i = i + abs(lhs) + 4;
                int rhs = (*this)[i];
                if(rhs != lhs){
                    return false;
                }
                i = i + 4;
                lhs = (*this)[i];
            }
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () {
            size_type n = (size_type)N;
            if(n < sizeof(T) + 8){
                bad_alloc();
            }
            (*this)[0] = n - 8;
            (*this)[n - 4] = n - 8;
            assert(valid());}

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate (size_type x) {
            int i = 0;
            int memoryI = 0;
            int minimum = 8 + sizeof(T);
            size_type bytes = x * sizeof(T);
            if(bytes > N || bytes < 0 || (int)N < minimum){
                bad_alloc();
            }
            while((size_t)i < N){
                int sentinel = (*this)[i];
                if(sentinel >= (int)bytes){
                    if(sentinel - (int)bytes >= minimum){
                        int newSent = (int)bytes * -1;
                        (*this)[i] = newSent;
                        memoryI = i + 4;
                        i = i + (bytes + 4);
                        (*this)[i] = newSent;
                        i = i + 4;
                        (*this)[i] = sentinel - bytes - 8;
                        i = i + 4;
                        i = i + (sentinel - bytes - 8);
                        (*this)[i] = sentinel - bytes - 8;
                        i = i + 4;
                    }
                    else{
                        (*this)[i] = sentinel * -1;
                        memoryI = i + 4;
                        i = i + (sentinel + 4);
                        (*this)[i] = sentinel * -1;
                        i = i + 4;
                    }
                    break;
                }
                else{
                    i = i + (abs((int)sentinel) + 8);
                }
            }
            assert(valid());
            if(memoryI != 0){
                return(pointer)(&(*this)[memoryI]);
            }
            else{
                return nullptr;
            }
        }

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

        // ----------
        // deallocate
        // ----------

        //helper function used by deallocate
        //i is the index of where to deallocate and size
        //is the size of what we are deallocating.
        void coalesce(int i, int size){
            if(i + size + 4 != (int)N){
                int rhs = (*this)[i + size + 4];
                if(rhs > 0){
                    int nextSize = size + rhs + 8;
                    (*this)[i - 4] = nextSize;
                    (*this)[i + nextSize] = nextSize;
                    return;
                }
            }
            (*this)[i - 4] = size;
            (*this)[i + size] = size;
        }
        
        //getter method
        char* getMemory(){
            return a;
        }
        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * b points to what we are trying to deallocate. Uses coalesce 
         * helper method.
         */
        void deallocate (pointer b, size_type x) {
            if(b == nullptr){
                throw invalid_argument("Null pointer, unable to deallocate");
            }
            char* memory = (char*)b;
            int i = memory - a;
            if(i < 0 || i >= (int)N) {
                throw invalid_argument("Invalid pointer");
            }
            int size = (*this)[i - 4];
            if(size > 0){
                throw invalid_argument("Can't deallocate");
            }
            size = size * -1;
            coalesce(i, size);
            if(i - sizeof(int) != 0){
                int lhs = (*this)[i - 8];
                if(lhs > 0){
                    coalesce(i - (8 + lhs), lhs);
                }
            }
            assert(valid());}

        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}

        // -----
        // begin
        // -----

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator begin () {
            return iterator(&(*this)[0]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator begin () const {
            return const_iterator(&(*this)[0]);}

        // ---
        // end
        // ---

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator end () {
            return iterator(&(*this)[N]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator end () const {
            return const_iterator(&(*this)[N]);}};

#endif // Allocator_h
