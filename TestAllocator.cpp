// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);}}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);}}

TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);}                                         // fix test

TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 4;
    const value_type v = 12;
    const pointer    b = x.allocate(s);
    if(b != nullptr){
        x.construct(b, v);
        ASSERT_EQ(v, *b);
        x.destroy(b);
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 5;
    const value_type v = 13;
    const value_type w = 15;
    const pointer    b = x.allocate(s);
    if(b != nullptr){
        x.construct(b, v);
        x.construct(b, w);
        ASSERT_EQ(w, *b);
        x.destroy(b);
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    allocator_type   y;
    const size_type  s = 5;
    const value_type v = 13;
    const value_type w = 15;
    const pointer    b = x.allocate(s);
    const pointer    c = y.allocate(s);
    if(b != nullptr){
        x.construct(b, v);
        y.construct(c, w);
        ASSERT_EQ(v, *b);
        ASSERT_EQ(w, *c);
        ASSERT_NE(*b, *c);
        x.destroy(b);
        x.deallocate(b, s);
        y.destroy(c);
        y.deallocate(c, s);
    }
}

TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 5;
    const value_type v = 13;
    const pointer    b = x.allocate(s);
    if(b != nullptr){
        pointer p = b + s;
        pointer c = b;
        try{
            while(c != p){
                x.construct(c, v);
                ++c;
            }
        }
        catch(...){
            while(b != c){
                --c;
                x.destroy(c);
            }
            x.deallocate(b,s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, p, v));
        while(b != p){
            --p;
            x.destroy(p);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[996], 992);
}

TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}

TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(10);                                      // read-only
    ASSERT_EQ(x[0], -80);
}

TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(7);                                     
    ASSERT_EQ(x[0], -56);
} 

TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(4);
    double* b = x.allocate(5);
    x.allocate(6);
    x.deallocate(b, 5);
    x.deallocate(a, 4);
    ASSERT_EQ(x[0], 80);
} 

TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(6);
    double* b = x.allocate(8);
    x.allocate(3);
    x.deallocate(b, 8);
    x.deallocate(a, 6);
    ASSERT_EQ(x[0], 120);
} 

TEST(AllocatorFixture, test14) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(10);
    x.deallocate(a, 10);
    ASSERT_EQ(x[0], 992);
} 

TEST(AllocatorFixture, test15) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(10);
    double* b = x.allocate(5);
    x.deallocate(a, 10);
    x.deallocate(b, 5);
    ASSERT_EQ(x[0], 992);
} 

TEST(AllocatorFixture, test16) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(6);
    x.allocate(2);
    x.allocate(4);
    ASSERT_EQ(x[0], -48);
} 

TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(1);
    x.allocate(3);
    ASSERT_EQ(x[0], -8);
} 

TEST(AllocatorFixture, test18) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5);
    x.allocate(8);
    ASSERT_EQ(x[0], -40);
}

TEST(AllocatorFixture, test19) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(3);
    x.allocate(3);
    x.allocate(3);                                     
    ASSERT_EQ(x[0], -24);
}

TEST(AllocatorFixture, test20) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5);
    x.allocate(3);
    ASSERT_EQ(x[0], -40);
}